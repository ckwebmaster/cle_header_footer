
// usage: log('inside coolFunc', this, arguments);
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function(){
  log.history = log.history || [];   // store logs to an array for reference
  log.history.push(arguments);
  if(this.console) {
    arguments.callee = arguments.callee.caller;
    var newarr = [].slice.call(arguments);
    (typeof console.log === 'object' ? log.apply.call(console.log, console, newarr) : console.log.apply(console, newarr));
  }
};

// make it safe to use console.log always
(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();){b[a]=b[a]||c}})((function(){try
{console.log();return window.console;}catch(err){return window.console={};}})());


// place any jQuery/helper plugins in here, instead of separate, slower script files.

/**
* hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne brian(at)cherne(dot)net
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type=="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.bind('mouseenter',handleHover).bind('mouseleave',handleHover)}})(jQuery);

/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

/**
 * Create a cookie with the given name and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String name The name of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */



/*
 * jQuery Selectbox plugin 0.2
 *
 * Copyright 2011-2012, Dimitar Ivanov (http://www.bulgaria-web-developers.com/projects/javascript/selectbox/)
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) license.
 * 
 * Date: Tue Jul 17 19:58:36 2012 +0300
 */
(function(d,f){var e="selectbox",c=false,b=true;function a(){this._state=[];this._defaults={classHolder:"sbHolder",classHolderDisabled:"sbHolderDisabled",classSelector:"sbSelector",classOptions:"sbOptions",classGroup:"sbGroup",classSub:"sbSub",classDisabled:"sbDisabled",classToggleOpen:"sbToggleOpen",classToggle:"sbToggle",classFocus:"sbFocus",speed:200,effect:"slide",onChange:null,onOpen:null,onClose:null}}d.extend(a.prototype,{_isOpenSelectbox:function(h){if(!h){return c}var g=this._getInst(h);return g.isOpen},_isDisabledSelectbox:function(h){if(!h){return c}var g=this._getInst(h);return g.isDisabled},_attachSelectbox:function(o,k){if(this._getInst(o)){return c}var j=d(o),u=this,m=u._newInst(j),l,r,i,p,v=c,t=j.find("optgroup"),h=j.find("option"),n=h.length;j.attr("sb",m.uid);d.extend(m.settings,u._defaults,k);u._state[m.uid]=c;j.hide();function g(){var w,x,s=this.attr("id").split("_")[1];for(w in u._state){if(w!==s){if(u._state.hasOwnProperty(w)){x=d("select[sb='"+w+"']")[0];if(x){u._closeSelectbox(x)}}}}}l=d("<div>",{id:"sbHolder_"+m.uid,"class":m.settings.classHolder,tabindex:j.attr("tabindex")});r=d("<a>",{id:"sbSelector_"+m.uid,href:"#","class":m.settings.classSelector,click:function(w){w.preventDefault();g.apply(d(this),[]);var s=d(this).attr("id").split("_")[1];if(u._state[s]){u._closeSelectbox(o)}else{u._openSelectbox(o)}}});i=d("<a>",{id:"sbToggle_"+m.uid,href:"#","class":m.settings.classToggle,click:function(w){w.preventDefault();g.apply(d(this),[]);var s=d(this).attr("id").split("_")[1];if(u._state[s]){u._closeSelectbox(o)}else{u._openSelectbox(o)}}});i.appendTo(l);p=d("<ul>",{id:"sbOptions_"+m.uid,"class":m.settings.classOptions,css:{display:"none"}});j.children().each(function(x){var y=d(this),s,w={};if(y.is("option")){q(y)}else{if(y.is("optgroup")){s=d("<li>");d("<span>",{text:y.attr("label")}).addClass(m.settings.classGroup).appendTo(s);s.appendTo(p);if(y.is(":disabled")){w.disabled=true}w.sub=true;q(y.find("option"),w)}}});function q(){var w=arguments[1]&&arguments[1].sub?true:false,s=arguments[1]&&arguments[1].disabled?true:false;arguments[0].each(function(y){var z=d(this),x=d("<li>"),A;if(z.is(":selected")){r.html("<span>"+z.text()+"</span>");v=b}if(y===n-1){x.addClass("last")}if(!z.is(":disabled")&&!s){A=d("<a>",{href:"#"+z.val(),rel:z.val()}).text(z.text()).bind("click.sb",function(E){if(E&&E.preventDefault){E.preventDefault()}var C=i,D=d(this),B=C.attr("id").split("_")[1];u._changeSelectbox(o,D.attr("rel"),D.text());u._closeSelectbox(o)}).bind("mouseover.sb",function(){var B=d(this);B.parent().siblings().find("a").removeClass(m.settings.classFocus);B.addClass(m.settings.classFocus)}).bind("mouseout.sb",function(){d(this).removeClass(m.settings.classFocus)});if(w){A.addClass(m.settings.classSub)}if(z.is(":selected")){A.addClass(m.settings.classFocus)}A.appendTo(x)}else{A=d("<span>",{text:z.text()}).addClass(m.settings.classDisabled);if(w){A.addClass(m.settings.classSub)}A.appendTo(x)}x.appendTo(p)})}if(!v){r.html("<span>"+h.first().text()+"</span>")}d.data(o,e,m);l.data("uid",m.uid).bind("keydown.sb",function(A){var D=A.charCode?A.charCode:A.keyCode?A.keyCode:0,C=d(this),z=C.data("uid"),y=C.siblings("select[sb='"+z+"']").data(e),B=C.siblings(["select[sb='",z,"']"].join("")).get(0),s=C.find("ul").find("a."+y.settings.classFocus);switch(D){case 37:case 38:if(s.length>0){var w;d("a",C).removeClass(y.settings.classFocus);w=s.parent().prevAll("li:has(a)").eq(0).find("a");if(w.length>0){w.addClass(y.settings.classFocus).focus();d("#sbSelector_"+z).html("<span>"+w.text()+"</span>")}}break;case 39:case 40:var w;d("a",C).removeClass(y.settings.classFocus);if(s.length>0){w=s.parent().nextAll("li:has(a)").eq(0).find("a")}else{w=C.find("ul").find("a").eq(0)}if(w.length>0){w.addClass(y.settings.classFocus).focus();d("#sbSelector_"+z).html("<span>"+w.text()+"</span>")}break;case 13:if(s.length>0){u._changeSelectbox(B,s.attr("rel"),s.text())}u._closeSelectbox(B);break;case 9:if(B){var y=u._getInst(B);if(y){if(s.length>0){u._changeSelectbox(B,s.attr("rel"),s.text())}u._closeSelectbox(B)}}var x=parseInt(C.attr("tabindex"),10);if(!A.shiftKey){x++}else{x--}d("*[tabindex='"+x+"']").focus();break;case 27:u._closeSelectbox(B);break}A.stopPropagation();return false}).delegate("a","mouseover",function(s){d(this).addClass(m.settings.classFocus)}).delegate("a","mouseout",function(s){d(this).removeClass(m.settings.classFocus)});r.appendTo(l);p.appendTo(l);l.insertAfter(j);d("html").live("mousedown",function(s){s.stopPropagation();d("select").selectbox("close")});d([".",m.settings.classHolder,", .",m.settings.classSelector].join("")).mousedown(function(s){s.stopPropagation()})},_detachSelectbox:function(h){var g=this._getInst(h);if(!g){return c}d("#sbHolder_"+g.uid).remove();d.data(h,e,null);d(h).show()},_changeSelectbox:function(j,i,k){var g,h=this._getInst(j);if(h){g=this._get(h,"onChange");d("#sbSelector_"+h.uid).html("<span>"+k+"</span>")}i=i.replace(/\'/g,"\\'");d(j).find("option[value='"+i+"']").attr("selected",b);if(h&&g){g.apply((h.input?h.input[0]:null),[i,h])}else{if(h&&h.input){h.input.trigger("change")}}},_enableSelectbox:function(h){var g=this._getInst(h);if(!g||!g.isDisabled){return c}d("#sbHolder_"+g.uid).removeClass(g.settings.classHolderDisabled);g.isDisabled=c;d.data(h,e,g)},_disableSelectbox:function(h){var g=this._getInst(h);if(!g||g.isDisabled){return c}d("#sbHolder_"+g.uid).addClass(g.settings.classHolderDisabled);g.isDisabled=b;d.data(h,e,g)},_optionSelectbox:function(j,g,i){var h=this._getInst(j);if(!h){return c}h[g]=i;d.data(j,e,h)},_openSelectbox:function(m){var k=this._getInst(m);if(!k||k.isOpen||k.isDisabled){return}var h=d("#sbOptions_"+k.uid),l=parseInt(d(window).height(),10),j=d("#sbHolder_"+k.uid).offset(),g=d(window).scrollTop(),o=h.prev().height(),n=l-(j.top-g)-o/2,i=this._get(k,"onOpen");h.css({top:o+"px",maxHeight:(n-o)+"px"});k.settings.effect==="fade"?h.fadeIn(k.settings.speed):h.slideDown(k.settings.speed);d("#sbToggle_"+k.uid).addClass(k.settings.classToggleOpen);this._state[k.uid]=b;k.isOpen=b;if(i){i.apply((k.input?k.input[0]:null),[k])}d.data(m,e,k)},_closeSelectbox:function(i){var h=this._getInst(i);if(!h||!h.isOpen){return}var g=this._get(h,"onClose");h.settings.effect==="fade"?d("#sbOptions_"+h.uid).fadeOut(h.settings.speed):d("#sbOptions_"+h.uid).slideUp(h.settings.speed);d("#sbToggle_"+h.uid).removeClass(h.settings.classToggleOpen);this._state[h.uid]=c;h.isOpen=c;if(g){g.apply((h.input?h.input[0]:null),[h])}d.data(i,e,h)},_newInst:function(g){var h=g[0].id.replace(/([^A-Za-z0-9_-])/g,"\\\\$1");return{id:h,input:g,uid:Math.floor(Math.random()*99999999),isOpen:c,isDisabled:c,settings:{}}},_getInst:function(h){try{return d.data(h,e)}catch(g){throw"Missing instance data for this selectbox"}},_get:function(h,g){return h.settings[g]!==f?h.settings[g]:this._defaults[g]}});d.fn.selectbox=function(h){var g=Array.prototype.slice.call(arguments,1);if(typeof h=="string"&&h=="isDisabled"){return d.selectbox["_"+h+"Selectbox"].apply(d.selectbox,[this[0]].concat(g))}if(h=="option"&&arguments.length==2&&typeof arguments[1]=="string"){return d.selectbox["_"+h+"Selectbox"].apply(d.selectbox,[this[0]].concat(g))}return this.each(function(){typeof h=="string"?d.selectbox["_"+h+"Selectbox"].apply(d.selectbox,[this].concat(g)):d.selectbox._attachSelectbox(this,h)})};d.selectbox=new a();d.selectbox.version="0.2"})(jQuery);
/*
jQuery.ThreeDots.min

Author Jeremy Horn
Version 1.0.10
Date: 1/25/2009
More: http://tpgblog.com/ThreeDots/
compiled by http://yui.2clics.net/
*/

(function(e){e.fn.ThreeDots=function(h){var g=this;if((typeof h=="object")||(h==undefined)){e.fn.ThreeDots.the_selected=this;var g=e.fn.ThreeDots.update(h)}return g};e.fn.ThreeDots.update=function(u){var k,t=null;var m,j,s,q,o;var l,i;var r,h,n;if((typeof u=="object")||(u==undefined)){e.fn.ThreeDots.c_settings=e.extend({},e.fn.ThreeDots.settings,u);var p=e.fn.ThreeDots.c_settings.max_rows;if(p<1){return e.fn.ThreeDots.the_selected}var g=false;jQuery.each(e.fn.ThreeDots.c_settings.valid_delimiters,function(v,w){if(((new String(w)).length==1)){g=true}});if(g==false){return e.fn.ThreeDots.the_selected}e.fn.ThreeDots.the_selected.each(function(){k=e(this);if(e(k).children("."+e.fn.ThreeDots.c_settings.text_span_class).length==0){return true}l=e(k).children("."+e.fn.ThreeDots.c_settings.text_span_class).get(0);var y=a(k,true);var x=e(l).text();d(k,l,y);var v=e(l).text();if((h=e(k).attr("threedots"))!=undefined){e(l).text(h);e(k).children("."+e.fn.ThreeDots.c_settings.e_span_class).remove()}r=e(l).text();if(r.length<=0){r=""}e(k).attr("threedots",x);if(a(k,y)>p){curr_ellipsis=e(k).append('<span style="white-space:nowrap" class="'+e.fn.ThreeDots.c_settings.e_span_class+'">'+e.fn.ThreeDots.c_settings.ellipsis_string+"</span>");while(a(k,y)>p){i=b(e(l).text());e(l).text(i.updated_string);t=i.word;n=i.del;if(n==null){break}}if(t!=null){var w=c(k,y);if((a(k,y)<=p-1)||(w)||(!e.fn.ThreeDots.c_settings.whole_word)){r=e(l).text();if(i.del!=null){e(l).text(r+n)}if(a(k,y)>p){e(l).text(r)}else{e(l).text(e(l).text()+t);if((a(k,y)>p+1)||(!e.fn.ThreeDots.c_settings.whole_word)||(v==t)||w){while((a(k,y)>p)){if(e(l).text().length>0){e(l).text(e(l).text().substr(0,e(l).text().length-1))}else{break}}}}}}}if(x==e(e(k).children("."+e.fn.ThreeDots.c_settings.text_span_class).get(0)).text()){e(k).children("."+e.fn.ThreeDots.c_settings.e_span_class).remove()}else{if((e(k).children("."+e.fn.ThreeDots.c_settings.e_span_class)).length>0){if(e.fn.ThreeDots.c_settings.alt_text_t){e(k).children("."+e.fn.ThreeDots.c_settings.text_span_class).attr("title",x)}if(e.fn.ThreeDots.c_settings.alt_text_e){e(k).children("."+e.fn.ThreeDots.c_settings.e_span_class).attr("title",x)}}}})}return e.fn.ThreeDots.the_selected};e.fn.ThreeDots.settings={valid_delimiters:[" ",",","."],ellipsis_string:"...",max_rows:2,text_span_class:"ellipsis_text",e_span_class:"threedots_ellipsis",whole_word:true,allow_dangle:false,alt_text_e:false,alt_text_t:false};function c(k,h){if(e.fn.ThreeDots.c_settings.allow_dangle==true){return false}var l=e(k).children("."+e.fn.ThreeDots.c_settings.e_span_class).get(0);var g=e(l).css("display");var i=a(k,h);e(l).css("display","none");var j=a(k,h);e(l).css("display",g);if(i>j){return true}else{return false}}function a(i,j){var g=typeof j;if((g=="object")||(g==undefined)){return e(i).height()/j.lh}else{if(g=="boolean"){var h=f(e(i));return{lh:h}}}}function b(k){var j;var i=e.fn.ThreeDots.c_settings.valid_delimiters;k=jQuery.trim(k);var g=-1;var h=null;var l=null;jQuery.each(i,function(m,o){if(((new String(o)).length!=1)||(o==null)){return false}var n=k.lastIndexOf(o);if(n!=-1){if(n>g){g=n;h=k.substring(g+1);l=o}}});if(g>0){return{updated_string:jQuery.trim(k.substring(0,g)),word:h,del:l}}else{return{updated_string:"",word:jQuery.trim(k),del:null}}}function f(h){e(h).append("<div id='temp_ellipsis_div' style='position:absolute; visibility:hidden'>H</div>");var g=e("#temp_ellipsis_div").height();e("#temp_ellipsis_div").remove();return g}function d(k,l,m){var q=e(l).text();var i=q;var o=e.fn.ThreeDots.c_settings.max_rows;var h,g,n,r,j;var p;if(a(k,m)<=o){return}else{p=0;curr_length=i.length;curr_middle=Math.floor((curr_length-p)/2);h=q.substring(p,p+curr_middle);g=q.substring(p+curr_middle);while(curr_middle!=0){e(l).text(h);if(a(k,m)<=(o)){j=Math.floor(g.length/2);n=g.substring(0,j);p=h.length;i=h+n;curr_length=i.length;e(l).text(i)}else{i=h;curr_length=i.length}curr_middle=Math.floor((curr_length-p)/2);h=q.substring(0,p+curr_middle);g=q.substring(p+curr_middle)}}}})(jQuery);

$(document).ready(function(){function b(){var c=document.createElement("input");return"placeholder" in c}if(b()){}else{var a=$(":input[placeholder]");a.each(function(){var c=$(this).attr("placeholder");$(this).attr("value",c)});a.focus(function(){var c=$(this).attr("value");var d=$(this).attr("placeholder");if(c==d){$(this).attr("value","")}});a.blur(function(){var c=$(this).attr("value");var d=$(this).attr("placeholder");if(c==""){$(this).attr("value",d)}});$("form").submit(function(){var c=$(":input");c.each(function(){if($(this).attr("value")==$(this).attr("placeholder")){$(this).attr("value","")}})})}});

/**
 * Get the value of a cookie with the given name.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String name The name of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};