/* Author:

*/
//Code for overloading the :contains selector to be case insensitive:

// New selector
jQuery.expr[':'].Contains = function(a, i, m) {
 return jQuery(a).text().toUpperCase()
     .indexOf(m[3].toUpperCase()) >= 0;
};

// Overwrites old selecor
jQuery.expr[':'].contains = function(a, i, m) {
 return jQuery(a).text().toUpperCase()
     .indexOf(m[3].toUpperCase()) >= 0;
};


$(function () {

	/* custom dropdown for the search box on the lading page.
	*************************************************************/
	if($('.search select').length){
		$('.search select').selectbox();
	}

	/* chat button to open librarian chat.
	********************************************************/
	if($('.chat').length){
		$('.chat-btn').click(function(e){
			$('.chat').slideToggle();
			$(this).toggleClass('chat-btn-active');
			e.preventDefault();
		})
	}

	/* Ellipsis for library news feed
	*********************************************************************/
	if($('#libCol2 .event p').length){
		$('#libCol2 .event p .description').ThreeDots({
			max_rows: 3
		})
	}

	/* Library Databases live search of page text
	*********************************************************/
	if($('.keywords')){
		var resultsFocus = false;
		$('.keywords').keyup(function(){
			if($('.keywords').val().length != 0){
				$('.db_results ul').empty();
				if(!$('.db_results').is(':visible')){
					$('.db_results').show();
				}
				$('#database_list p:contains("'+$('.keywords').val()+'"), #database_list h5:contains("'+$('.keywords').val()+'")').each(function(){
					var title = ($(this).get(0).tagName === 'H5') ? $(this).text().split(' ').join('-').toLowerCase() : $(this).parent().find('h5').text().split(' ').join('-').toLowerCase(),
						liText = ($(this).get(0).tagName === 'H5') ? $(this).text() : $(this).parent().find('h5').text(),
						docURL = window.location.pathname.split('#');
					title = title.replace(/'/g,'');

					if(!$('.db_results ul li[data-title='+title+']').length){
						$('.db_results ul').append('<li data-title="'+title+'"><a href="'+document.URL.split('#')[0]+'#'+title+'">'+liText+'</a></li>');
					}
				})
			}else{
				$('.db_results').hide();
				$('.db_results ul').empty();
			}
		})
		$('.db_results').hover(function(){
			resultsFocus = true;
		}, function(){
			resultsFocus = false;
		})
		$('.keywords').blur(function(){
			if(!resultsFocus){
				$('.db_results').hide();
			}
		})
	}

//	try {

	try {
		$("#links").bind('mouseover', function() {
			$("#list").show();
		});

		$("#links").bind('mouseout', function() {
			$("#list").hide();
		});

/*		$("#links").mouseover(function() {
			$("#list").show();
		}).mouseout(function () {
			$("#list").hide();
		});*/
	} catch (e) {
		// Fail
	}

		$("#askbtn").live('click', function() {
//			if (!$("html").hasClass('ie7')) {
				$("#expandible_block").toggleClass('open');
			//	$("#expandible_block > div.holder").slideToggle();
//			}
		});

		$("#stories li").syncHeight();

		// $("#content > section").syncHeight();

		$("#masthead.expandible .hidden_content section").syncHeight();

		$('.callout_feed img').removeAttr('height').removeAttr('width');

		$("#question").focus(function() {
			if (!$.data(document.body, 'askdefault')) {
				$.data(document.body, 'askdefault', $(this).val());
			}
			if ($(this).val() == $.data(document.body, 'askdefault')) {
				$(this).val('');
			}
		}).blur(function() {
			if ($(this).val() == '') {
				$(this).val($.data(document.body, 'askdefault'));
			}
		});

		$("#header_search").focus(function() {
			if (!$.data(document.body, 'searchdefault')) {
				$.data(document.body, 'searchdefault', $(this).val());
			}
			if ($(this).val() == $.data(document.body, 'searchdefault')) {
				$(this).val('');
			}
		}).blur(function() {
			if ($(this).val() == '') {
				$(this).val($.data(document.body, 'searchdefault'));
			}
		});

		$("#ask_adm_mini_text").focus(function() {
			if (!$.data(document.body, 'askminidefault')) {
				$.data(document.body, 'askminidefault', $(this).val());
			}
			if ($(this).val() == $.data(document.body, 'askminidefault')) {
				$(this).val('');
			}
		}).blur(function() {
			if ($(this).val() == '') {
				$(this).val($.data(document.body, 'askminidefault'));
			}
		});

		$("#alert_tab").live('click', function() {
			$("#alert_message").toggleClass('open');
			if ($("#alert_message").hasClass('open')) {
				$.cookie('alert_state', 'open');
			} else {
				$.cookie('alert_state', 'closed');
			}
		});

		if ($("#stories li").length > 2) {
			$("#stories").bxSlider({
				mode: 'horizontal',
				controls: true,
				displaySlideQty: 2,
				moveSlideQty: 2,
				prevText: 'Prev',
				nextText: 'Next',
				nextSelector: '#news .next',
				prevSelector: '#news .prev',
				hideControlOnEnd: true
			});
		}

		$(".js-hide").each(function() {
			$(this).removeClass("js-hide");
		});

		// Controls for image slideshow in page masthead
		$("#masthead_slider").bxSlider({
			controls: false,
		/* /auto: false, 	//	*/
		/**/auto: true,
			pause: 12000,
			speed: 4000, 	//  */
			pager: true,
			mode: 'fade' // 'horizontal', 'vertical', or 'fade'
		});

		var homeBxOptions = {
			controls: true,
		/* /auto: false, 	//	*/
		/**/auto: true,
			pause: 12000,   // CHANGED FROM 9000 to 12000 on OCT 18, 2013 (mm)
			speed: 2000, 	//  */
			pager: true,
			mode: 'fade', // 'horizontal', 'vertical', or 'fade'
			onSliderLoad: function() {
			}
		};

		$(".slide-open .homepage_gallery").each(function () {

			if ($(this).find('li').length > 1) {

				$(this).bxSlider(homeBxOptions);

			}

		});

		if ($('#featured').length) {

			$('#featured').on('zchange', function() {

				$(".slide-open .homepage_gallery").each(function () {

					console.log($(this), $(this).find('li').length, $(this).parents('.slideshow').find('.bx-wrapper').length);

					if ($(this).find('li').length > 1 && $(this).parents('.slideshow').find('.bx-wrapper').length === 0) {

						$(this).bxSlider(homeBxOptions);

					}

				});

			});

		}

	
 
        
		// Controls for image slideshow in home page tab
		// $("#homepage_gallery").bxSlider({
		// 	controls: true,
		// /*  auto: false, 	//	*/
		// /**/auto: true,
		// 	pause: 12000,   // CHANGED FROM 9000 to 12000 on OCT 18, 2013 (mm)
		// 	speed: 2000, 	//  */
		// 	pager: true,
		// 	mode: 'fade', // 'horizontal', 'vertical', or 'fade'
		// 	onSliderLoad: function() {
		// 	}
		// });

		$(".faculty_profile_slider").bxSlider({
			controls: false,
			auto: false,
			pager: ($(".faculty_profile_slider > li").length > 1) ? true: false,
			mode: 'fade'
		});

//		try {
		$("article img.caption").captify({
			// all of these options are... optional
			// ---
			// 'fade', 'slide', 'always-on'
			animation: 'always-on',
			// text/html to be placed at the beginning of every caption
			prefix: '',
			// opacity of the caption on mouse over
			opacity: '1',
			// the name of the CSS class to apply to the caption box
			className: 'caption-bottom',
			// position of the caption (top or bottom)
			position: 'bottom',
			// caption span % of the image
			spanWidth: '100%'
		});
//		} catch(e) {

//		}

		$("#smaller, #larger").live('click', function() {
			$("#column_container_1 article, #column_container_2 article, #column_container_3 article, .gallery_nav, #faculty_alpha_display, #faculty_list, #story_list, #faculty_search_box, #faculty_search, .faculty_expandible, #column_container_1, #column_container_2, #contact_info, .callout_socialmediaicons, #expandibles, .callout_feed, .callout_button, #popular, #news_year_display").toggleClass('zoom');
			$("#smaller, #larger").toggleClass('enabled');
		});

		$("#ask_adm_mini_btn").live('click', function() {
			$("#ask_adm_mini").toggleClass('open');
			if ($("#ask_adm_mini").hasClass('open')) {
				$.cookie('askadmmini_state', 'open');
			} else {
				$.cookie('askadmmini_state', 'closed');
			}
		});

		$(".expandme", "#expandibles li").live('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('showmore showless');
			if ($(this).hasClass('showmore')) {
				$(this).html('Show More');
			}
			if ($(this).hasClass('showless')) {
				$(this).html('Show Less');
			}
			$(this).parent().toggleClass('show');
		});

		$(".expandme", ".faculty_expandible").live('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('showmore showless');
			if ($(this).hasClass('showmore')) {
				$(this).html('Show More');
			}
			if ($(this).hasClass('showless')) {
				$(this).html('Show Less');
			}
			$(this).parent().find('.hidden_content').toggle();
		});

		$(".expandme", "#masthead.expandible .content").live('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('showmore showless');
			if ($(this).hasClass('showmore')) {
				$(this).html('Show More');
			}
			if ($(this).hasClass('showless')) {
				$(this).html('Show Less');
			}
			$('.hidden_content', "#masthead.expandible").toggle();
			$("#masthead.expandible .hidden_content section").syncHeight();
		});

		$("#faculty_search_box .expandme").live('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('showmore showless');
			$("#faculty_search").toggle();
		});

		$(".callout_feed h3 a").live('click', function(e) {
			e.preventDefault();
			var parenttmp = $(this).parent();
			$('.callout_container',parenttmp.parent()).toggle();
			parenttmp.toggleClass('closed');
		});
		// Commented out by imoffitt
		// $(".callout_feed h3 a").trigger('click');

		$('a.refine').live('click', function(e) {
			var parenttmp = $(this).parent();
			parenttmp.parent().find('.form_row_item.refine').show();
			parenttmp.remove();
		});

		/*
		---------------------------
		Course Search javascript
		---------------------------
		*/

		/*$('#courses').superPager({
			pageSize: 1,
			topPaging: true,
			bottomPaging: false,
			action: false
		});*/

		$('#course_search_toggle .cs_toggle').live('click', function() {
			$('#course_search_toggle .form, #course_search_toggle .switch').toggleClass('show hide');
		});

		// Progressive enhancement
		$('div.jd_program input[name="interest"]').each(function() { var clone = $("<input type='checkbox' id='clone' name='clone' value='clone'>"); clone.attr('id',$(this).attr('id')).attr('name',$(this).attr('name')).attr('value',$(this).attr('value')); clone.prependTo($(this).parent()); $(this).remove(); });
		$('div.jd_program input[name="typeofcourse"]').each(function() { var clone = $("<input type='checkbox' id='clone' name='clone' value='clone'>"); clone.attr('id',$(this).attr('id')).attr('name',$(this).attr('name')).attr('value',$(this).attr('value')); clone.prependTo($(this).parent()); $(this).remove(); });
		$('div.ipmmp input[name="interest"]').each(function() { var clone = $("<input type='checkbox' id='clone' name='clone' value='clone'>"); clone.attr('id',$(this).attr('id')).attr('name',$(this).attr('name')).attr('value',$(this).attr('value')); clone.prependTo($(this).parent()); $(this).remove(); });

		$('#course_search input[type="radio"][name="degree"]').live('click', function() {
		//	console.log($(this).val());
			switch ($(this).val()) {
				default: 				// Default to JD Program
				case "jd_program":
					var showClass = '.jd_program';
					break;
				case "llm_program":
					var showClass = '.llm_program';
					break;
				case "ipmmp":
					var showClass = '.ipmmp';
					break;
			}

			$('#course_search .form_group').hide();
			$('#course_search .form_group input').attr('disabled','disabled');

			$('#course_search .form_group'+showClass+' input').removeAttr('disabled');
			$('#course_search .form_group'+showClass).show();

			$('#course_search input:checked').removeAttr('checked');
			$(this).attr('checked', 'checked');

		});

		$('#course_search').live('submit', function() {

			$('<input name="focus" id="course-focus" value="" type="hidden" />').appendTo("#course_search");
			$('<input name="course_type" id="course-type" value="" type="hidden" />').appendTo("#course_search");

			var tmp = [];
			$('#course_search input[name="interest"]:checked').each(function(index) {
				tmp.push($(this).val());
			});

			$('#course-focus').val(tmp.join('|'));
			// console.log($('#course-focus').val());

			var tmp = [];
			$('#course_search input[name="typeofcourse"]:checked').each(function(index) {
				tmp.push($(this).val());
			});

			$('#course-type').val(tmp.join('|'));
			// console.log($('#course-type').val());

			$('#course_search input[name="interest"]').attr('disabled', 'disabled');
			$('#course_search input[name="typeofcourse"]').attr('disabled', 'disabled');

			return true;

		});

		/*
		---------------------------
		END Course Search javascript
		---------------------------
		*/

		$('#faculty_list .faculty:odd').addClass('odd');

		$("#header_search_form").live('submit', function(e) {
		  if ($("#header_search").val() == '' || $("#header_search").val() == 'Search Chicago-Kent') {
			e.preventDefault();
			$("#header_search").val('');
			$(this).submit();
		  }
		});

		$('#news_index_year').live('change', function() {
			$("#news_search").submit();
		});

		// Overlay prettyPhoto title fix. Window shows "undefined" if no title attribute is given to the link.
		$("a.lightbox, a.overlay, a[href*='.swf']").each(function() {
			if (!$(this).attr('title')) {
				$(this).attr('title', '');
			}
		});

		$("a[href*='.swf']", '#container').prettyPhoto({
			opacity: 0.90, /* Value between 0 and 1 */
			show_title: false, /* true/false */
			theme: 'pp_ck', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			default_height: 450,
			default_width: 600,
		//	horizontal_padding: 0, /* The padding on each side of the picture */
		//	autoplay: true, /* Automatically start videos: True/False */
			hideflash: true,
			modal: true,
			social_tools: '' /* html or false to disable */
		});

		$("a.lightbox, a.overlay").prettyPhoto({
			opacity: 0.90, /* Value between 0 and 1 */
			show_title: false, /* true/false */
			theme: 'pp_ck', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			default_height: 450,
			default_width: 600,
		//	horizontal_padding: 0, /* The padding on each side of the picture */
		//	autoplay: true, /* Automatically start videos: True/False */
			hideflash: true,
			modal: true,
			social_tools: '', /* html or false to disable */
			changepicturecallback: function() {

			}
		});

		$('.icon a', '#main.level').qtip({
			show: 'mouseover',
			hide: 'mouseout',
			style: { name: 'dark', tip: true },
			position: {
				corner: {
					target: 'topMiddle',
					tooltip: 'bottomMiddle'
				}
			}
		});

		// Iframe z-index fix
		$('iframe', '#homepage_feature').each(function(){
			var url = $(this).attr("src");
			$(this).attr("src",url+"?wmode=transparent");
		});

		$('.slide', '#featured .slide .content a').live('touchstart touchend', function(e) {
			$(this).click();
			return false;
		});

/*	} catch (e) {
		// Ignore
	}
*/

		if($('.feeds').length){
			var feeds = $('.feeds').text(),
				eURL = encodeURIComponent(feeds),
				url = "http://query.yahooapis.com/v1/public/yql?q=select%20channel.item.title%2Cchannel.item.link%2C%20channel.item.pubDate%0A%20%20%20%20from%20xml%20where%20url%20in("+eURL+")%0A%20%20%7C%20unique(field%3D%22channel.item.link%22)%0A%20%20%7C%20sort(field%3D%22channel.item.pubDate%22%2C%20descending%3D%22true%22)%20%7C%20truncate(count%3D10)&format=json&callback=?",
				html = '';
			$.getJSON(url, function(data){
				$.each(data.query.results.rss, function(i){
					if(data.query.count == 1){
						var item = data.query.results.rss,
							item = item.channel.item;
					}else{
						var item = data.query.results.rss[i],
							item = item.channel.item;
					}
					var linkTitle = (item.link) ? '<a href="'+item.link+'">'+item.title+'</a>' : item.title,
					date = new Date(item.pubDate),
					months = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
					fDate = months[date.getMonth()]+' '+date.getDate() + ", " + date.getFullYear(),
					fDate = (months[date.getMonth()] != undefined && !isNaN(date.getDate()) && !isNaN(date.getFullYear())) ? '<span class="date-from">'+fDate+'</span>' : '';


					$('#newsFeed').append('<p class="item">'+linkTitle+'<br class="clearfix" />'+fDate+'</p>');
				})
			})
		}


	/*
		Faculty Experts
	 */
	$('#faculty-search-again .clear-button').on('click', function (e) {
		$('#faculty-expertise').val('');
		$('#again-keyword').val('');
		e.preventDefault();
	});

	$('#faculty-search input[type="submit"], #faculty-search-again input[type="submit"]').on('click', function (e) {
		e.preventDefault();

		var expert = $('#faculty-expertise option:selected').val();
		var keyword = $('input.faculty-keyword').val();

		if (expert != "" || keyword != "") {
			$(this).parents('form').submit();
		} else {
			$(this).after('<div class="errormsg">Choose an expertise or enter keyword.</div>');
			setTimeout(function () { $(".errormsg").remove(); }, 5000);
		}
	});

	// jTabs init
	//$('#tabs').jTabs();


});
			

